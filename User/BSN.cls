/// Class for working with Dutch citizen identification numbers (<i>Burger Service Nummer</i>).
Class User.BSN
{

/// Returns 1 if <var>bsn</var> is a valid BSN, 0 if not.
ClassMethod CheckBSN(bsn As %String) As %Boolean
{
	set bsn = +bsn
	if $length(bsn)=8 set bsn = "0" _ bsn
	if $length(bsn)'=9 quit 0
	set check = -(bsn # 10)
	for i=1:1:8 set check = $extract(bsn, *-i) * (i + 1) + check
	quit '(check # 11)
}

/// Generates a random, valid BSN.
ClassMethod GenerateBSN() As %String
{
	// Pick a random 6 or 7-digit number.
	set rnd = 100000 + $random(9899999)
	set residu = 0
	// Fix the length at 7 digits.
	if $length(rnd)=6 set rnd = "0" _ rnd
	// Calculate the weighted sum for the these 7 digits.
	for i=1:1:7 set residu = $extract(rnd, i) * (10 - i) + residu
	// For the 8th position there is one 'forbidden' value f that will not produce a valid BSN.
	set residu = residu # 11
	if '(residu#2) set residu = residu + 11
	set f = (21 - residu) \ 2
	// Pick an 8th-position digit unequal to the 'forbidden' value f.
	set d = ($random(9) + f + 1) # 10
	// Calculate the digit for the last position.
	set last = (2 * d + residu) # 11
	quit rnd _ d _ last
}

/// Generates a BSN and tests it.
ClassMethod Test()
{
	set bsn = ..GenerateBSN()
	write !, bsn
	write !, ..CheckBSN(bsn)
}

}
