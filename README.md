# BSN check for Ensemble

BSN is the Dutch citizen identification number.

The **CheckBSN** method validates a BSN string by checking if it passes the so-called _eleven test_.

The **GenerateBSN** method generates an arbitrary valid BSN. This is useful for populating test data.
